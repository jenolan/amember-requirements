<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Requirements</title>

    <link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqJJREFUeNp0U0tPE1EU/ubV6QBtKQaQQnBhgiYmElKJJgaVECIxMZroxoULN7B05V6XygqjRl2ZqH9AXSigsjCRBMrCjZZECKUWqFpKmZl23p47nQ5q9Ey+ObnnnHse372Xw99ysQm40ry35jBC/+uEFKFAmIKHt3vucKMCXCBwZBIIUuh6fW346lnLsiCKIp7MPXtDtrGGU8TdBBAT/uzC9QCLdGD+9qOgNglKi+7oqu9j4rCfBx7/ExZoExxvcnppJitJEqaXZrNsHdh9v/iPrY8ocRr1Ar50JjtimqYhGW2NbWvl20FchjBRT1B1AbnRjJe+fPpSularwfM8H6ZpQlVVDB481qcoClzXxcv5VwEH/jxUqkZJxDpxu7u7qFQqYUs8z8OoauWFD7M5tj7SP9jrjxEmCGeunwtjnKEh0WgUnz9l8lVdu89aX/w4N46IlEFE9BOM0Kw3gk4mfeU4MC0Tuq5Dr+qQIzI0tWKQ63GQc4ICwCBgVJ4aHRo7l2hp3b+5WRhIxttilmvvK24XoZs6dWrDEzwYqlpybdsJEtyEIKYgSRkOd2KLQyeH06waI0g3dKyWVsFzPERBDA7Dg61WyzvrGz4HLd0dvWpJyzmW0y9SiQLHcWnTMNTMwnxWao7EYn3JPhSscml+PW+VDUNMROTm4+09rQMHjvonw77vao4RKeCMVFxbWeksbmysk/2WW3NOJQ63pUov1pbtinmPzesajmNtVQ/Jg/FORMhCMPL6lme6D0W8s9jDqD+OHroL3RxshebmvMZlqWt2TX57Y/6xZ20yvzejhDihHU9rXdix4UQdNJ1v76Ww8SB83F/TWwuhEp/PjRRjSWa8EOK+66uzvPMgL/nbFD7sQJ/5eQIzyDUa8HLuF1JdvwQYAL+KPe88BpnxAAAAAElFTkSuQmCC" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="robots" content="noindex, nofollow" />

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <style>

        /* Sticky footer styles
        -------------------------------------------------- */

        html,
        body {
          height: 100%;
          /* The html and body elements cannot have any padding or margin. */
        }

        /* Wrapper for page content to push down footer */
        #wrap {
          min-height: 100%;
          height: auto !important;
          height: 100%;
          /* Negative indent footer by its height */
          margin: 0 auto -60px;
          /* Pad bottom by footer height */
          padding: 0 0 60px;
        }

        /* Set the fixed height of the footer here */
        #footer {
          height: 60px;
          background-color: #f5f5f5;
        }

        /* Custom page CSS
        -------------------------------------------------- */

        #header {
            text-align: center;
            margin-bottom: 40px;
        }

        #header h1 {
            font-size: 56px;
        }

        #footer p {
            margin: 20px 0;
        }

    </style>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    </head>
    <body>

        <div id="wrap">
            <div class="container">
                <div id="header">
                    <h1>Requirements Checker</h1>
                    <h2>Select the application to run the test for below</h2>
                </div>

                <table class="table">
                    <tbody>
                            <tr>
                                <td><a href='aMember_v5.php'>aMember v5.x</a></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><a href='aMember_v4.php'>aMember v4.x</a></td>
                                <td>&nbsp;</td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8">
                        <p><a href='https://jlogica.com'>JLogica</a> - Quality Code Always!</p>
                    </div>
                    <div class="col-xs-4">
                        <p class="text-right"><a target="_blank" href="https://github.com/JLogica/aMember.Requirements.PHP">View or report issues in GitHub</a></p>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>