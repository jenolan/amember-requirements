<?php
$cfg = array(   'aMember.Guess.Path'      => '%Root%/amember,%Root%/member',
                'Wordpress.Guess.Path'    => '%Root%,%Root%/wp,%Root%/wordpress',
			);
foreach( $cfg AS $k => $v )
{
	$cfg[$k] = str_replace( '%Root%', $_SERVER['DOCUMENT_ROOT'], $v );
}
return( $cfg );